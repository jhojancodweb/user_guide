<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
    <head>
        <title>CODWEB - USER GUIDE</title>
        <!-- Meta -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">    
        <link rel="shortcut icon" href="<?php echo site_url() . 'assets/uploads/files/favicon.ico'; ?>">  
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <!-- Global CSS -->
        <link rel="stylesheet" href="<?php echo site_url(); ?>plugins/bootstrap/css/bootstrap.min.css">   
        <!-- Plugins CSS -->    
        <link rel="stylesheet" href="<?php echo site_url(); ?>plugins/font-awesome/css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo site_url(); ?>plugins/elegant_font/css/style.css">

        <!-- Theme CSS -->
        <link id="theme-style" rel="stylesheet" href="<?php echo site_url(); ?>css/styles.css">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head> 

    <body class="landing-page">   
        <div class="page-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <h3>
                        Select User Guide
                    </h3>
                </div>
            </div>
            <div class="btn-group" role="group" aria-label="...">
                <div class="btn-group" role="group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        User Guide
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <?php foreach ($info as $data) { ?>
                            <li><a href="<?php echo site_url('docs/user_guide/' . $data->id_info); ?>"><?php echo $data->info_name; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="btn-group" role="group" aria-label="...">
                <div class="btn-group" role="group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Generate User Guide
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <?php foreach ($info as $data) { ?>
                            <li><a href="<?php echo site_url('admin/generate/' . $data->id_info.'/'.$data->info_name); ?>"><?php echo $data->info_name; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>

        </div><!--//page-wrapper-->

        <footer class="footer text-center">
            <div class="container">
                <!--/* This template is released under the Creative Commons Attribution 3.0 License. Please keep the attribution link below when using for your own project. Thank you for your support. :) If you'd like to use the template without the attribution, you can check out other license options via our website: themes.3rdwavemedia.com */-->
                <small class="copyright">Powered by <a href="http://codweb.co" target="_blank">Codweb</a></small>

            </div><!--//container-->
        </footer><!--//footer-->


        <!-- Main Javascript -->          
        <script type="text/javascript" src="<?php echo site_url(); ?>plugins/jquery-1.12.3.min.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>plugins/bootstrap/js/bootstrap.min.js"></script>                                                                     
        <script type="text/javascript" src="<?php echo site_url(); ?>plugins/jquery-match-height/jquery.matchHeight-min.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>js/main.js"></script>

    </body>
</html> 

