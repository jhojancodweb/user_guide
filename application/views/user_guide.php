<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
    <head>
        <title>PQRS SOFTWARE - MANUALES</title>
        <!-- Meta -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">    
        <link rel="shortcut icon" href="<?php echo site_url().'uploads/'.$info->info_favicon; ?>">  
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <!-- Global CSS -->
        <link rel="stylesheet" href="<?php echo site_url(); ?>plugins/bootstrap/css/bootstrap.min.css">   
        <!-- Plugins CSS -->    
        <link rel="stylesheet" href="<?php echo site_url(); ?>plugins/font-awesome/css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo site_url(); ?>plugins/elegant_font/css/style.css">

        <!-- Theme CSS -->
        <link id="theme-style" rel="stylesheet" href="<?php echo site_url(); ?>css/styles.css">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head> 

    <body class="landing-page">   
        <div class="page-wrapper">
            <!-- ******Header****** -->
            <header class="header text-center">
                <div class="container">
                    <div class="branding">
                        <h1 class="logo">
                            <span aria-hidden="true" class="icon_documents_alt icon"></span>
                            <span class="text-highlight"><?php echo $info->info_name;?></span><span class="text-bold"><?php echo $info->info_subname;?></span>
                        </h1>
                    </div><!--//branding-->
                    <div class="tagline">
                        <p><?php echo $info->info_tiitle;?></p>
                        <p><?php echo $info->info_subtittle;?></p>
                    </div><!--//tagline-->
                </div><!--//container-->
            </header><!--//header-->

            <section class="cards-section text-center">
                <div class="container">
                    <img src="<?php echo site_url().'uploads/'.$info->info_logo; ?>"  />
                    <div class="intro">
                        <p><?php echo $info->info_descripcion;?></p>
                    </div><!--//intro-->
                    <div id="cards-wrapper" class="cards-wrapper row">
                        <?php foreach ($guides as $guide) { ?>
                            <div class="item item-<?php echo $guide->guides_color; ?> col-md-4 col-sm-6 col-xs-6">
                                <div class="item-inner">
                                    <div class="icon-holder">
                                        <i class="icon fa <?php echo $guide->guides_icon; ?>"></i>
                                    </div><!--//icon-holder-->
                                    <h3 class="title"><?php echo $guide->guides_name; ?></h3>
                                    <p class="intro"><?php echo $guide->guides_descripcion; ?></p>
                                    <a class="link" href="<?php echo site_url() . 'docs/guide/' . $guide->id_guides.'/'.$user_guide; ?>"><span></span></a>
                                </div><!--//item-inner-->
                            </div><!--//item-->
                        <?php } ?>
                    </div><!--//cards-->

                </div><!--//container-->
            </section><!--//cards-section-->
        </div><!--//page-wrapper-->

        <footer class="footer text-center">
            <div class="container">
                <!--/* This template is released under the Creative Commons Attribution 3.0 License. Please keep the attribution link below when using for your own project. Thank you for your support. :) If you'd like to use the template without the attribution, you can check out other license options via our website: themes.3rdwavemedia.com */-->
                <small class="copyright">Powered by <a href="http://codweb.co" target="_blank">Codweb</a></small>

            </div><!--//container-->
        </footer><!--//footer-->


        <!-- Main Javascript -->          
        <script type="text/javascript" src="<?php echo site_url(); ?>plugins/jquery-1.12.3.min.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>plugins/bootstrap/js/bootstrap.min.js"></script>                                                                     
        <script type="text/javascript" src="<?php echo site_url(); ?>plugins/jquery-match-height/jquery.matchHeight-min.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>js/main.js"></script>

    </body>
</html> 

