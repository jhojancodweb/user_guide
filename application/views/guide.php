<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
    <head>
        <title>PQRS SOFTWARE - <?php echo $guide->guides_name; ?></title>
        <!-- Meta -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">    
        <link rel="shortcut icon" href="<?php echo site_url().'uploads/'.$info->info_favicon; ?>">  
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <!-- Global CSS -->
        <link rel="stylesheet" href="<?php echo site_url(); ?>plugins/bootstrap/css/bootstrap.min.css">   
        <!-- Plugins CSS -->    
        <link rel="stylesheet" href="<?php echo site_url(); ?>plugins/font-awesome/css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo site_url(); ?>plugins/prism/prism.css">
        <link rel="stylesheet" href="<?php echo site_url(); ?>plugins/elegant_font/css/style.css">

        <!-- Theme CSS -->
        <link id="theme-style" rel="stylesheet" href="<?php echo site_url(); ?>css/styles.css">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head> 

    <body class="body-<?php echo $guide->guides_color; ?>">
        <div class="page-wrapper">
            <!-- ******Header****** -->
            <header id="header" class="header">
                <div class="container">
                    <div class="branding">
                        <h1 class="logo">
                            <a href="<?php echo site_url('docs/user_guide/'.$user_guide); ?>">
                                <span aria-hidden="true" class="icon_documents_alt icon"></span>
                                <span class="text-highlight"><?php echo $info->info_name;?></span><span class="text-bold"><?php echo $info->info_subname;?></span>
                            </a>
                        </h1>
                    </div><!--//branding-->
                    <ol class="breadcrumb">
                        <li><a href="<?php echo site_url('docs/user_guide/'.$user_guide); ?>">Inicio</a></li>
                        <li class="active"><?php echo $guide->guides_name; ?></li>
                    </ol>
                </div><!--//container-->
            </header><!--//header-->
            <div class="doc-wrapper">
                <div class="container">
                    <div id="doc-header" class="doc-header text-center">
                        <h1 class="doc-title"><i class="icon fa <?php echo $guide->guides_icon; ?>"></i> <?php echo $guide->guides_name; ?></h1>
                    </div><!--//doc-header-->
                    <div class="doc-body">
                        <div class="doc-content">
                            <div class="content-inner">
                                <?php foreach ($topics as $topic) { ?>
                                    <section id="section_<?php echo $topic->id_topics; ?>" class="doc-section">
                                        <h2 class="section-title"><?php echo $topic->topics_name; ?></h2>
                                        <?php echo ($topic->topics_text != NULL) ? '<div class="section-block">' . $topic->topics_text . '</div>' : ''; ?>
                                        <?php
                                        if ($sections[$topic->id_topics] != FALSE) {
                                            foreach ($sections[$topic->id_topics] as $section) {
                                                ?>
                                                <div id="step_<?php echo $section->id_sections; ?>"  class="section-block">
                                                    <h3 class="block-title"><?php echo $section->sections_name; ?></h3>           
                                                    <?php echo $section->sections_text; ?>
                                                </div>

                                                <?php
                                            }
                                        }
                                        ?>
                                    </section>
                                <?php } ?>
                            </div><!--//content-inner-->
                        </div><!--//doc-content-->
                        <div class="doc-sidebar hidden-xs">
                            <nav id="doc-nav">
                                <ul id="doc-menu" class="nav doc-menu" data-spy="affix">

                                    <?php foreach ($topics as $topic) { ?>
                                        <li>
                                            <a class="scrollto" href="#section_<?php echo $topic->id_topics; ?>"><?php echo $topic->topics_name; ?></a>
                                            <?php if ($sections[$topic->id_topics] != FALSE) { ?>
                                                <ul class="nav doc-sub-menu">
                                                    <?php foreach ($sections[$topic->id_topics] as $section) { ?>
                                                        <li><a class="scrollto" href="#step_<?php echo $section->id_sections; ?>"><?php echo $section->sections_name; ?></a></li>
                                                    <?php } ?>
                                                </ul>
                                            <?php } ?>
                                        </li>
                                    <?php } ?>
                                </ul><!--//doc-menu-->
                            </nav>
                        </div><!--//doc-sidebar-->
                    </div><!--//doc-body-->              
                </div><!--//container-->
            </div><!--//doc-wrapper-->
        </div><!--//page-wrapper-->
        <footer id="footer" class="footer text-center">
            <div class="container">
                <!--/* This template is released under the Creative Commons Attribution 3.0 License. Please keep the attribution link below when using for your own project. Thank you for your support. :) If you'd like to use the template without the attribution, you can check out other license options via our website: themes.3rdwavemedia.com */-->
                <small class="copyright">Powered by <a href="http://codweb.co" target="_blank">Codweb</a></small>
            </div><!--//container-->
        </footer><!--//footer-->
        <!-- Main Javascript -->          
        <script type="text/javascript" src="<?php echo site_url(); ?>plugins/jquery-1.12.3.min.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>plugins/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>plugins/prism/prism.js"></script>    
        <script type="text/javascript" src="<?php echo site_url(); ?>plugins/jquery-scrollTo/jquery.scrollTo.min.js"></script>                                                                
        <script type="text/javascript" src="<?php echo site_url(); ?>plugins/jquery-match-height/jquery.matchHeight-min.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>js/main.js"></script>

    </body>
</html> 

