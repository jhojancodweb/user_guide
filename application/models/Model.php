<?php

class Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function get_info() {
        $query = $this->db->get('info');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function get_info_unique($id) {
        $this->db->where('id_info', $id);
        $query = $this->db->get('info');
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function get_guides($id) {
        $this->db->where('id_info', $id);
        $query = $this->db->get('guides');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function get_guide($id) {
        $this->db->where('id_guides', $id);
        $query = $this->db->get('guides');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    function get_topics($id) {
        $this->db->where('id_guides', $id);
        $query = $this->db->get('topics');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    function get_sections($id) {
        $this->db->where('id_topics', $id);
        $query = $this->db->get('sections');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

}
