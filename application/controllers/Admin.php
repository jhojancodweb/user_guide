<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->database();
        $this->load->helper('url');
        $this->load->library('grocery_CRUD');
        $this->load->model('model');
    }

    public function _example_output($output = null) {
        $this->load->view('example.php', $output);
    }

    public function offices() {
        $output = $this->grocery_crud->render();

        $this->_example_output($output);
    }

    public function index() {
        $this->_example_output((object) array('output' => '', 'js_files' => array(), 'css_files' => array()));
    }

    public function guides() {
        try {
            $crud = new grocery_CRUD();
            $crud->set_table('guides');
            $crud->set_subject('Guides');
            //$crud->unset_texteditor('guides_descripcion');
            $crud->set_relation('id_info', 'info', '{info_name}');
            $crud->add_action('More', ' ', 'admin/topics', 'ui-icon-plus');
            $output = $crud->render();
            $this->_example_output($output);
        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }
    }

    public function topics($id = NULL) {
        $crud = new grocery_CRUD();
        if ($id != NULL) {
            $crud->where('topics.id_guides', $id);
        }
        $crud->set_table('topics');
        $crud->set_relation('id_guides', 'guides', '{guides_name} - {id_info}');
        $crud->set_subject('Topics');
        //$crud->unset_texteditor('topics_text');
        $crud->add_action('More', ' ', 'admin/sections', 'ui-icon-plus');
        $output = $crud->render();
        $this->_example_output($output);
    }

    public function sections($id = NULL) {
        $crud = new grocery_CRUD();
        if ($id != NULL) {
            $crud->where('sections.id_topics', $id);
        }
        $crud->set_table('sections');
        $crud->set_subject('Sections');
        $crud->set_relation('id_topics', 'topics', '{topics_name} - {id_guides}');
        //$crud->unset_texteditor('sections_text');
        $output = $crud->render();
        $this->_example_output($output);
    }

    public function info() {
        $crud = new grocery_CRUD();

        $crud->set_table('info');
        $crud->set_subject('Info');
        $crud->set_field_upload('info_favicon', 'uploads');
        $crud->set_field_upload('info_logo', 'uploads');
        $crud->unset_texteditor('info_descripcion');


        $output = $crud->render();

        $this->_example_output($output);
    }

    public function generate($id, $name) {
        $this->load->library('zip');
        echo "Eliminando Directorio <br>";
        sleep(1);
        $this->rrmdir('./output/' . strtolower($name));
        echo "Creando Directorio <br>";
        sleep(1);
        mkdir('./output/' . strtolower($name));
        $folder = './output/' . strtolower($name);
        echo "Copiando Carpetas <br>";
        sleep(1);
        $this->copy_directory('./css', $folder . '/css');
        $this->copy_directory('./js', $folder . '/js');
        $this->copy_directory('./images', $folder . '/images');
        $this->copy_directory('./plugins', $folder . '/plugins');
        $this->copy_directory('./uploads', $folder . '/uploads');
        echo "Leyendo el Index <br>";
        sleep(1);
        $pagina_inicio = file_get_contents(site_url('docs/user_guide/' . $id));
        $pagina_inicio = str_replace(site_url(), "./", $pagina_inicio);
        $data['guides'] = $this->model->get_guides($id);
        foreach ($data['guides'] as $guide) {
            echo "Leyendo Guias <br>";
            sleep(1);
            $pagina_inicio = str_replace('docs/guide/' . $guide->id_guides . '/' . $id, $guide->id_guides . ".html", $pagina_inicio);
            $pagina = file_get_contents(site_url('docs/guide/' . $guide->id_guides . '/' . $id));
            $pagina = str_replace(site_url('docs/user_guide/' . $id), "./", $pagina);
            $pagina = str_replace(site_url(), "./", $pagina);
            $pagina = str_replace('../../../', "./", $pagina);
            $pagina = str_replace('href="./"', 'href="./index.html"', $pagina);
            file_put_contents($folder . '/' . $guide->id_guides . '.html', $pagina);
        }
        echo "Creando HTML <br>";
        sleep(1);
        file_put_contents($folder . '/index.html', $pagina_inicio);
        echo "Sitio Generado<br>Ruta: " . site_url('outputs') . '/' . $name;
       /* $this->zip->read_dir($folder);
        $this->zip->download($name.'.zip');*/
    }

    function copy_directory($src, $dst) {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ( $file = readdir($dir))) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if (is_dir($src . '/' . $file)) {
                    $this->copy_directory($src . '/' . $file, $dst . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }

    function rrmdir($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir")
                        $this->rrmdir($dir . "/" . $object);
                    else
                        unlink($dir . "/" . $object);
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }

}
