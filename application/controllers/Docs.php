<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Docs extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('model');
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        $data['info'] = $this->model->get_info();
        $this->load->view('index', $data);
    }

    public function user_guide($id) {
        $data['info'] = $this->model->get_info_unique($id);
        $data['guides'] = $this->model->get_guides($id);
        $data['user_guide'] = $id;
        $this->load->view('user_guide', $data);
    }

    public function guide($id, $user_guide) {
        $data['info'] = $this->model->get_info_unique($user_guide);
        $data['guide'] = $this->model->get_guide($id);
        $data['topics'] = $this->model->get_topics($id);
        $data['user_guide'] = $user_guide;
        foreach ($data['topics'] as $dato) {
            $data['sections'][$dato->id_topics] = $this->model->get_sections($dato->id_topics);
        }
        $this->load->view('guide', $data);
    }

}
